package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import administradorSolicitudes.AdministradorSolicitudes;
import view.PanelInicial;

public class PanelInicialController  implements ActionListener
{
	PanelInicial panelInicial;

	public PanelInicialController(PanelInicial panelInicial)
	{
		this.panelInicial=panelInicial;
	}



	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getActionCommand().equals("CARGAR IMAGEN"))
		{
			System.out.println("CARGAR IMAGEN");

			this.panelInicial.abrirExploradorArchivo();

		}
		if (e.getActionCommand().equals("CONTINUAR"))
		{
			System.out.println("CONTINUAR");
			try {
				AdministradorSolicitudes.analizarVotante(this.panelInicial.getExplorador().getSelectedFile().getAbsolutePath(),"Carnet");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 

		}
				
	}
	
	
	
	
	
	
	
}
