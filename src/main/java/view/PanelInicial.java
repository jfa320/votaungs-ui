package view;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import controller.PanelInicialController;

import javax.swing.JEditorPane;

public class PanelInicial extends JPanel 
{
	private JButton cargarImagen;
	private JButton continuar;
	private JFileChooser explorador;
	private JEditorPane textoCaja;
	private PanelInicialController ventanaInicialController;

	public PanelInicial() 
	{
		ventanaInicialController= new PanelInicialController(this);
		cargarImagen = new JButton("CARGAR IMAGEN");

		cargarImagen.setBounds(56, 118, 156, 23);
		
		this.continuar = new JButton("CONTINUAR");
		continuar.setBounds(56, 154, 156, 23);
		continuar.addActionListener(ventanaInicialController);
		
		this.textoCaja = new JEditorPane();
		this.textoCaja.setBounds(10, 60, 450, 20);



		setLayout(null);
		add(cargarImagen);
		add(continuar);
		add(this.textoCaja);
		cargarImagen.addActionListener(ventanaInicialController);
		explorador = new JFileChooser();		
	}


	public JButton getCargarImagen() {
		return cargarImagen;
	}


	public JFileChooser getExplorador() {
		return explorador;
	}


	public void abrirExploradorArchivo() 
	{
		this.explorador.showOpenDialog(explorador);
		this.textoCaja.setText(this.explorador.getSelectedFile().toString());

	}
}
