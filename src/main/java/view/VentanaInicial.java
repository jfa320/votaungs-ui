package view;

import javax.swing.JFrame;


public class VentanaInicial {

	private JFrame frame;
	private PanelInicial panelInicial;

	public VentanaInicial() 
	{	
		
		initialize();
	}

	private void initialize() 
	{	
		
		panelInicial= new PanelInicial();	
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);					
	}

	public JFrame getFrame() 
	{
		return frame;
	}

	
	public void setPanelInicial()
	{
		panelInicial.setVisible(false);		
		panelInicial.setBounds(51, 29, 267, 184);
		frame.getContentPane().add(panelInicial);
		panelInicial.setLayout(null);
		frame.add(panelInicial);
		panelInicial.setVisible(true);
	}




	
	
	
}
